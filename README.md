﻿# SetDebugDateFormat

## Component Information
* Author: Jonathan Hult
* Last Updated: build_1_20160110
* License: MIT

## Overview
This WebCenter Content component allows the debugging date to be overridden by that specified in the SetDebugDateFormat_DateFormat preference prompt.

* Filters:
	- postConfigComponentTables: Core - SetDebugDateFormat - Alters the date format for debugging output using the date format set in SetDebugDateFormat_DateFormat.

* Preference Prompts: 
	- SetDebugDateFormat_ComponentEnabled: Boolean determining if the component functionality is enabled.
	- SetDebugDateFormat_DateFormat: Date format to use for debugging.

## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 11.1.1.8.0PSU-2014-05-08 00:14:10Z-r117806 (Build:7.3.5.185)

## Changelog
* build_1_20160110
	- Initial component release