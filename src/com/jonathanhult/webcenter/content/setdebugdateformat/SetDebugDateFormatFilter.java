package com.jonathanhult.webcenter.content.setdebugdateformat;

import static intradoc.shared.SharedObjects.getEnvValueAsBoolean;
import static intradoc.shared.SharedObjects.getEnvironmentValue;
import intradoc.common.ExecutionContext;
import intradoc.common.LocaleUtils;
import intradoc.common.Report;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.shared.FilterImplementor;

import java.text.SimpleDateFormat;

public class SetDebugDateFormatFilter implements FilterImplementor {

	final static String TRACE_SECTION = "setdebugdateformat";
	final static String COMPONENT_ENABLED = "SetDebugDateFormat_ComponentEnabled";
	final static String DATE_FORMAT_PREF_PROMPT = "SetDebugDateFormat_DateFormat";

	public int doFilter(final Workspace ws, final DataBinder binder, final ExecutionContext ctx) throws DataException, ServiceException {
		if (getEnvValueAsBoolean(COMPONENT_ENABLED, false)) {
			trace("SetDebugDateFormat enabled");

			final String dateFormat = getEnvironmentValue(DATE_FORMAT_PREF_PROMPT);
			trace("SetDebugDateFormat_DateFormat: " + dateFormat);

			if (dateFormat != null && !dateFormat.isEmpty()) {
				try {
					LocaleUtils.m_traceLogFormat = new SimpleDateFormat(dateFormat);
                    trace("Successfully set above date format");
				} catch (IllegalArgumentException e) {
					trace(String.format("Invalid date format specified in %s preference prompt: %s", DATE_FORMAT_PREF_PROMPT, dateFormat));
				}
			}
		}
		return CONTINUE;
	}

	private void trace(final String message) {
		Report.trace(TRACE_SECTION, message, null);
	}
}
